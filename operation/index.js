module.exports = {
    get Adapter () {
        return require('./Adapter');
    },

    get Operation () {
        return require('./Operation');
    },

    get Operationable () {
        return require('./Operationable');
    }
};
