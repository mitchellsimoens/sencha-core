module.exports = {
    get Listener () {
        return require('./Listener');
    },

    get Observable () {
        return require('./Observable');
    }
};
