module.exports = {
    get Base () {
        return require('./Base');
    },

    get Class () {
        return require('./Class');
    },

    get ClassMixin () {
        return require('./ClassMixin')
    },

    get Config () {
        return require('./Config');
    },

    get Deferrable () {
        return require('./Deferrable');
    },

    get Deferred () {
        return require('./Deferred');
    },

    get Managerable () {
        return require('./Managerable');
    },

    get Mixin () {
        return require('./Mixin');
    },

    get combiner () {
        return require('./combiner/');
    },

    get event () {
        return require('./event');
    },

    get operation () {
        return require('./operation/');
    }
};
